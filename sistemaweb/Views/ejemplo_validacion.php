<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Formulario Web</title>
    <link rel="stylesheet" href="../Resources/css/bootstrap.min.css">

</head>
<h1> Formulario de Validación de Salud </h1>
<body>
    <!--Barra de Navegacion-->
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Cambiar Navegacion</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">AplicacionWeb</a>
        </div>

    </nav>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <div class="alert alert-danger text-center" style="display:none;" id="error">
                            <strong>Adventencia: </strong>Debe completar todos los campos
                        </div>
                        <div class="alert alert-success text-center" style="display:none;" id="exito">
                            <strong>Felicidades: </strong>Su registro ha sido guardado
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nombre1" class="control-label col-xs-4">Nombre 1:</label>
                        <div class="col-xs-5">
                            <input id="nombre1" name="nombre1" type="text" class="form-control" placeholder="Escriba su Nombre" onkeyup="validacion('nombre1');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nombre2" class="control-label col-xs-4">Nombre 2:</label>
                        <div class="col-xs-5">
                            <input id="nombre2" name="nombre2" type="text" class="form-control" placeholder="Escriba su Nombre" onkeyup="validacion('nombre2');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellido1" class="control-label col-xs-4">Apellido 1:</label>
                        <div class="col-xs-5">
                            <input id="apellido1" name="apellido1" type="text" class="form-control" placeholder="Escriba su Apellido" onkeyup="validacion('apellido1');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellido2" class="control-label col-xs-4">Apellido 2:</label>
                        <div class="col-xs-5">
                            <input id="apellido2" name="apellido2" type="text" class="form-control" placeholder="Escriba su Apellido" onkeyup="validacion('apellido2');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fechanacimiento" class="control-label col-xs-4">Fecha de nacimiento:</label>
                        <div class="col-xs-5">
                            <input id="fechanacimiento" name="fechanacimiento" type="text" class="form-control" placeholder="Escriba su Fecha de nacimiento" onkeyup="validacion('fechanacimiento');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tipodedocumento" class="control-label col-xs-4">Tipo de Documento:</label>
                        <div class="col-xs-5">
                            <select id="tipodedocumento" name="tipodedocumento" class="form-control" onchange="validacion('tipodedocumento');">
                                <option value="">Seleccione uno</option>
                                <option value="1">Cedula de Ciudadania</option>
                                <option value="2">Tarjeta de Identidad</option>
                                <option value="3">Cedula de Extranjeria</option>
                                <option value="4">Registro Civil</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="documento" class="control-label col-xs-4">Numero de Documento:</label>
                        <div class="col-xs-5">
                            <input id="documento" name="documento" type="text" class="form-control" placeholder="Escriba su # de Documento" onkeyup="validacion('documento');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fechaexpedicion" class="control-label col-xs-4">Fecha de Expedición:</label>
                        <div class="col-xs-5">
                            <input id="fechaexpedicion" name="fechaexpedicion" type="text" class="form-control" placeholder="Escriba la Fecha de expedición" onkeyup="validacion('fechaexpedicion');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lugarnacimiento" class="control-label col-xs-4">Lugar de Nacimiento:</label>
                        <div class="col-xs-5">
                            <input id="lugarnacimiento" name="lugarnacimiento" type="text" class="form-control" placeholder="Escriba el lugar nacimiento" onkeyup="validacion('lugarnacimiento');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ciudadresidente" class="control-label col-xs-4">Ciudad de Residencia:</label>
                        <div class="col-xs-5">
                            <input id="ciudadresidente" name="ciudadresidente" type="text" class="form-control" placeholder="Escriba la ciudad en la que reside" onkeyup="validacion('ciudadresidente');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="localidad" class="control-label col-xs-4">Localidad:</label>
                        <div class="col-xs-5">
                            <select id="localidad" name="localidad" class="form-control" onchange="validacion('localidad');">
                                <option value="">Seleccione uno</option>
                                <option value="1">Usaquen</option>
                                <option value="2">Chapinero</option>
                                <option value="3">Santa Fe</option>
                                <option value="4">San Cristóbal</option>
                                <option value="5">Usme</option>
                                <option value="6">Tunjuelito</option>
                                <option value="7">Bosa</option>
                                <option value="8">Kennedy</option>
                                <option value="9">Fontibón</option>
                                <option value="10">Engativá</option>
                                <option value="11">Suba</option>
                                <option value="12">Barrios Unidos</option>
                                <option value="13">Teusaquillo</option>
                                <option value="14">Los Mártires</option>
                                <option value="15">Antonio Nariño</option>
                                <option value="16">Puente Aranda</option>
                                <option value="17">Candelaria</option>
                                <option value="18">Rafael Uribe</option>
                                <option value="19">Ciudad Bolívar</option>
                                <option value="20">Sumapáz</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="barrio" class="control-label col-xs-4">Barrio:</label>
                        <div class="col-xs-5">
                            <input id="barrio" name="barrio" type="text" class="form-control" placeholder="Escriba el barrio en el que reside" onkeyup="validacion('barrio');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="direccion" class="control-label col-xs-4">Dirección:</label>
                        <div class="col-xs-5">
                            <input id="direccion" name="direccion" type="text" class="form-control" placeholder="Escriba la dirección de su residencia" onkeyup="validacion('direccion');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telefono" class="control-label col-xs-4">Teléfono:</label>
                        <div class="col-xs-5">
                            <input id="telefono" name="telefono" type="text" class="form-control" placeholder="Escriba su # de Teléfono" onkeyup="validacion('telefono');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="celular" class="control-label col-xs-4">Celular:</label>
                        <div class="col-xs-5">
                            <input id="celular" name="celular" type="text" class="form-control" placeholder="Escriba su # de Celular" onkeyup="validacion('celular');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="correo" class="control-label col-xs-4">Correo:</label>
                        <div class="col-xs-5">
                            <input id="correo" name="correo" type="text" class="form-control" placeholder="Escriba su correo electrónico" onkeyup="validacion('correo');">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sangrecorrespondiente" class="control-label col-xs-4">RH:</label>
                        <div class="col-xs-3">
                            <label class="radio-inline">
                                <input id="sangre1" type="radio" name="sangrecorrespondiente" value="O+" onclick="validacion('sangrecorrespondiente')"> O+
                            </label>
                            <label class="radio-inline">
                                <input id="sangre2" type="radio" name="sangrecorrespondiente" value="O-" onclick="validacion('sangrecorrespondiente')"> O-
                            </label>
                            <label class="radio-inline">
                                <input id="sangre3" type="radio" name="sangrecorrespondiente" value="A+" onclick="validacion('sangrecorrespondiente')"> A+
                            </label>
                            <label class="radio-inline">
                                <input id="sangre4" type="radio" name="sangrecorrespondiente" value="A-" onclick="validacion('sangrecorrespondiente')"> A-
                            </label>
                            <label class="radio-inline">
                                <input id="sangre5" type="radio" name="sangrecorrespondiente" value="B+" onclick="validacion('sangrecorrespondiente')"> B+
                            </label>
                            <label class="radio-inline">
                                <input id="sangre6" type="radio" name="sangrecorrespondiente" value="B-" onclick="validacion('sangrecorrespondiente')"> B-
                            </label>
                            <label class="radio-inline">
                                <input id="sangre7" type="radio" name="sangrecorrespondiente" value="AB+" onclick="validacion('sangrecorrespondiente')"> AB+
                            </label>
                        </div>
                        <div class="col-xs-2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="estrato" class="control-label col-xs-4">Estrato:</label>
                        <div class="col-xs-3">
                            <label class="radio-inline">
                                <input id="estrato1" type="radio" name="estrato" value="1" onclick="validacion('estrato')"> 1
                            </label>
                            <label class="radio-inline">
                                <input id="estrato2" type="radio" name="estrato" value="2" onclick="validacion('estrato')"> 2
                            </label>
                            <label class="radio-inline">
                                <input id="estrato3" type="radio" name="estrato" value="3" onclick="validacion('estrato')"> 3
                            </label>
                            <label class="radio-inline">
                                <input id="estrato4" type="radio" name="estrato" value="4" onclick="validacion('estrato')"> 4
                            </label>
                            <label class="radio-inline">
                                <input id="estrato5" type="radio" name="estrato" value="5" onclick="validacion('estrato')"> 5
                            </label>
                            <label class="radio-inline">
                                <input id="estrato6" type="radio" name="estrato" value="6" onclick="validacion('estrato')"> 6
                            </label>
                        </div>
                        <div class="col-xs-2">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="tipovinculacion" class="control-label col-xs-4">Tipo de Vinculación:</label>
                        <div class="col-xs-3">
                            <label class="radio-inline">
                                <input id="beneficiario" type="radio" name="tipovinculacion" value="B" onclick="validacion('tipovinculacion')"> Beneficiario
                            </label>
                            <label class="radio-inline">
                                <input id="cotizante" type="radio" name="tipovinculacion" value="C" onclick="validacion('tipovinculacion')"> Cotizante
                            </label>
                        </div>
                        <div class="col-xs-2">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="datosbeneficiario" class="control-label col-xs-4">Datos del Beneficiario:</label>
                        <div class="col-xs-5">
                            <select id="datosbeneficiario" name="datosbeneficiario" class="form-control" onchange="validacion('datosbeneficiario');">
                                <option value="">Seleccione el Parentesco</option>
                                <option value="1">Papá</option>
                                <option value="2">Mamá</option>
                                <option value="3">Tío</option>
                                <option value="4">Tía</option>
                                <option value="5">Abuela</option>
                                <option value="6">Abuelo</option>
                                <option value="7">Hermano</option>
                                <option value="8">Hermana</option>
                                <option value="9">Otro</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="codigo" class="control-label col-xs-4"></label>
                        <div class="col-xs-5">
                            <button type="button" class="btn btn-primary" onclick='verificar();'><span class="glyphicon glyphicon-lock"></span> Entrar</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <script type="text/javascript" src="../Resources/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../Resources/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var v=true;
        $("span.help-block").hide();

        function verificar(){

            var v1=0,v2=0,v3=0,v4=0,v5=0,v6=0,v7=0,v8=0,v9=0,v10=0,v11=0,v12=0,v13=0,v14=0,v15=0,v16=0,v17=0,v18=0,v19=0,v20=0,v21=0;
            v1=validacion('nombre1');
            v2=validacion('nombre2');
            v3=validacion('apellido1');
            v4=validacion('apellido2');
            v5=validacion('fechanacimiento');
            v6=validacion('tipodedocumento');
            v7=validacion('documento');
            v8=validacion('fechaexpedicion');
            v9=validacion('lugarexpedicion');
            v10=validacion('lugarnacimiento');
            v11=validacion('ciudadresidente');
            v12=validacion('localidad');
            v13=validacion('barrio');
            v14=validacion('direccion');
            v15=validacion('telefono');
            v16=validacion('celular');
            v17=validacion('correo');
            v18=validacion('sangrecorrespondiente');
            v19=validacion('estrato');
            v20=validacion('tipovinculacion');
            v21=validacion('datosbeneficiario');
            if (v1===false || v2===false || v3===false || v4===false || v5===false || v6===false || v7===false || v8===false || v9===false || v10===false || v11===false || v12===false || v13===false || v14===false || v15===false|| v16===false|| v17===false|| v18===false|| v19===false|| v20===false || v21===false) {
                 $("#exito").hide();
                 $("#error").show();
            }else{
                $("#error").hide();
                $("#exito").show();
            }
            /*total=v1+v2+v3+v4+v5+v6;
            if (v && total>=6) {
                $("#error").hide();
                $("#exito").show();
            }else{
                 $("#exito").hide();
                 $("#error").show();
            }


            validacion('nombres');
            validacion('dni');
            validacion('email');
            validacion('genderRadios');
            validacion('carrera');
            if (v) {
                alert("los campos estan validados")
            }
            else{
                alert("los campos no estan validados");
            }*/

        }

        function validacion(campo){
            var a=0;
            if (campo==='nombre1'){
                apellido = document.getElementById(campo).value;
                if( apellido == null || apellido.length == 0 || /^\s+$/.test(apellido) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }

            if (campo==='nombre2'){
                apellido = document.getElementById(campo).value;
                if( apellido == null || apellido.length == 0 || /^\s+$/.test(apellido) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }
            if (campo==='apellido1'){
                apellido1 = document.getElementById(campo).value;
                if( apellido1 == null || apellido1.length == 0 || /^\s+$/.test(apellido1) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }
            if (campo==='apellido2'){
                apellido2 = document.getElementById(campo).value;
                if( apellido2 == null || apellido2.length == 0 || /^\s+$/.test(apellido2) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }
            if (campo==='fechanacimiento'){
                fechanacimiento = document.getElementById(campo).value;
                if( fechanacimiento == null || fechanacimiento.length == 0 || /^\s+$/.test(fechanacimiento) ) {
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;
                }
                else
                {
                    if( isNaN(fechanacimiento) )
                    {
                        $("#glypcn"+campo).remove();
                        $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                        $('#'+campo).parent().children('span').text("No se aceptan letras").show();
                        $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                        return false;
                    }
                    else{
                        if (!(/^\d{8}$/.test(fechanacimiento)))
                        {
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                            $('#'+campo).parent().children('span').text("Debe ingresar los 8 digitos asi: dia,mes,año").show();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                            return false;
                        }
                        else{
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                            $('#'+campo).parent().children('span').hide();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");

                            return true;
                        }

                    }
                }
            }
            if (campo==='tipodedocumento'){
                tipodedocumento = document.getElementById(campo).selectedIndex;
                if( tipodedocumento == null || tipodedocumento == 0 ) {
                    $('#tipodedocumento').parent().parent().attr("class", "form-group has-error");
                    return false;
                }
                else{
                    $('#tipodedocumento').parent().parent().attr("class", "form-group has-success");
                    return true;

                }
            }
            if (campo==='documento'){
                documento = document.getElementById(campo).value;
                if( documento == null || documento.length == 0 || /^\s+$/.test(documento) ) {
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else
                {
                    if( isNaN(documento) )
                    {
                        $("#glypcn"+campo).remove();
                        $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                        $('#'+campo).parent().children('span').text("No se aceptan letras").show();
                        $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                        return false;
                    }
                    else{
                        if (!(/^\d{10 || 7}$/.test(documento)))
                        {
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                            $('#'+campo).parent().children('span').text("").show();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                            return false;
                        }
                        else{
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                            $('#'+campo).parent().children('span').hide();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");

                            return true;
                        }

                    }
                }
            }
            if (campo==='fechaexpedicion'){
                fechaexpedicion = document.getElementById(campo).value;
                if( fechaexpedicion == null || fechaexpedicion.length == 0 || /^\s+$/.test(fechaexpedicion) ) {
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else
                {
                    if( isNaN(fechaexpedicion) )
                    {
                        $("#glypcn"+campo).remove();
                        $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                        $('#'+campo).parent().children('span').text("No se aceptan letras").show();
                        $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                        return false;
                    }
                    else{
                        if (!(/^\d{8}$/.test(fechaexpedicion)))
                        {
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                            $('#'+campo).parent().children('span').text("Debe ingresar la fecha de expedicion así: dia,mes,año").show();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                            return false;
                        }
                        else{
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                            $('#'+campo).parent().children('span').hide();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");

                            return true;
                        }

                    }
                }
            }
            if (campo==='lugarnacimiento'){
                lugarnacimiento = document.getElementById(campo).value;
                if( lugarnacimiento == null || lugarnacimiento.length == 0 || /^\s+$/.test(lugarnacimiento) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }
            if (campo==='ciudadresidente'){
                ciudadresidente = document.getElementById(campo).value;
                if( ciudadresidente == null || ciudadresidente.length == 0 || /^\s+$/.test(ciudadresidente) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }
            if (campo==='localidad'){
                localidad = document.getElementById(campo).selectedIndex;
                if( localidad == null || localidad == 0 ) {
                    $('#localidad').parent().parent().attr("class", "form-group has-error");
                    return false;
                }
                else{
                    $('#localidad').parent().parent().attr("class", "form-group has-success");
                    return true;

                }
            }
            if (campo==='barrio'){
                barrio = document.getElementById(campo).value;
                if( barrio == null || barrio.length == 0 || /^\s+$/.test(barrio) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }
            if (campo==='direccion'){
                direccion = document.getElementById(campo).value;
                if( direccion == null || direccion.length == 0 || /^\s+$/.test(direccion) ) {

                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún carácter").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else{
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                    $('#'+campo).parent().children('span').hide();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");
                    return true;

                }
            }
            if (campo==='telefono'){
                telefono = document.getElementById(campo).value;
                if( telefono == null || telefono.length == 0 || /^\s+$/.test(telefono) ) {
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún número").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else
                {
                    if( isNaN(telefono) )
                    {
                        $("#glypcn"+campo).remove();
                        $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                        $('#'+campo).parent().children('span').text("No se aceptan letras").show();
                        $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                        return false;
                    }
                    else{
                        if (!(/^\d{7}$/.test(telefono)))
                        {
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                            $('#'+campo).parent().children('span').text("Ingrese los 7 números").show();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                            return false;
                        }
                        else{
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                            $('#'+campo).parent().children('span').hide();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");

                            return true;
                        }

                    }
                }
            }
            if (campo==='celular'){
                celular = document.getElementById(campo).value;
                if( celular == null || celular.length == 0 || /^\s+$/.test(celular) ) {
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún número").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;

                }
                else
                {
                    if( isNaN(celular) )
                    {
                        $("#glypcn"+campo).remove();
                        $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                        $('#'+campo).parent().children('span').text("No se aceptan letras").show();
                        $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                        return false;
                    }
                    else{
                        if (!(/^\d{10}$/.test(celular)))
                        {
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                            $('#'+campo).parent().children('span').text("Ingrese los 10 números de su celular").show();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                            return false;
                        }
                        else{
                            $("#glypcn"+campo).remove();
                            $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                            $('#'+campo).parent().children('span').hide();
                            $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");

                            return true;
                        }

                    }
                }
            }
            if (campo==='correo'){
                correo = document.getElementById(campo).value;
                if( correo == null || correo.length == 0 || /^\s+$/.test(correo) ) {
                    $("#glypcn"+campo).remove();
                    $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#'+campo).parent().children('span').text("Ingrese algún Correo electrónico").show();
                    $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");

                    return false;

                }
                else{
                    if (!(/\S+@\S+\.\S+/.test(correo))) {
                        $("#glypcn"+campo).remove();
                        $('#'+campo).parent().parent().attr("class", "form-group has-error has-feedback");
                        $('#'+campo).parent().children('span').text("Ingrese un Correo valido").show();
                        $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-remove form-control-feedback'></span>");
                        return false;
                    }
                    else{
                        $("#glypcn"+campo).remove();
                        $('#'+campo).parent().parent().attr("class", "form-group has-success has-feedback");
                        $('#'+campo).parent().children('span').hide();
                        $('#'+campo).parent().append("<span id='glypcn"+campo+"' class='glyphicon glyphicon-ok form-control-feedback'></span>");

                        return true;
                    }
                }

            }
            if (campo==='sangrecorrespondiente'){
                sangrecorrespondiente = document.getElementsByName(campo);
                var seleccionado = false;
                for(var i=0; i<sangrecorrespondiente.length; i++) {
                  if(sangrecorrespondiente[i].checked) {
                    seleccionado = true;
                    break;
                  }
                }

                if(!seleccionado) {
                    $('#sangre1').parent().parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#sangre1').parent().parent().next().append("<span id='sexo'  class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;
                }
                else{
                    $("#sexo").remove();
                    $('#sangre1').parent().parent().parent().attr("class", "form-group has-success");
                    return true;
                }
            }
            if (campo==='estrato'){
                estrato = document.getElementsByName(campo);
                var seleccionado = false;
                for(var i=0; i<estrato.length; i++) {
                  if(estrato[i].checked) {
                    seleccionado = true;
                    break;
                  }
                }
                if(!seleccionado) {
                    $('#estrato1').parent().parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#estrato1').parent().parent().next().append("<span id='clasesocial'  class='glyphicon glyphicon-remove form-control-feedback'></span>");
                    return false;
                }
                else{
                    $("#clasesocial").remove();
                    $('#estrato1').parent().parent().parent().attr("class", "form-group has-success");
                    return true;
                }
            }
            if (campo==='tipovinculacion'){
                tipovinculacion = document.getElementsByName(campo);
                var seleccionado = false;
                for(var i=0; i<tipovinculacion.length; i++) {
                  if(tipovinculacion[i].checked) {
                    seleccionado = true;
                    break;
                  }
                }
                if(!seleccionado) {
                    $('#beneficiario').parent().parent().parent().attr("class", "form-group has-error has-feedback");
                    $('#beneficiario').parent().parent().next().append("<span id='vincu' class='glyphicon glyphicon-remove form-control-feedback'></span>");


                    return false;
                }
                else{
                    $("#vincu").remove();
                    $('#beneficiario').parent().parent().parent().attr("class", "form-group has-success");
                    return true;
                }
            }
            if (campo==='datosbeneficiario'){
                datosbeneficiario = document.getElementById(campo).selectedIndex;
                if( datosbeneficiario == null || datosbeneficiario == 0 ) {
                    $('#datosbeneficiario').parent().parent().attr("class", "form-group has-error");
                    return false;
                }
                else{
                    $('#datosbeneficiario').parent().parent().attr("class", "form-group has-success");
                    return true;
                }
            }
        }
    </script>
</body>
</html>
